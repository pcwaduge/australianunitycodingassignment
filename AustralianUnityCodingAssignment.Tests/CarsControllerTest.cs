﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AustralianUnityCodingAssignment.Models;
using AustralianUnityCodingAssignment.Controllers;
using System.Web.Http.Results;
using System.Collections.Generic;

namespace AustralianUnityCodingAssignment.Tests
{
    /// <summary>
    /// A test class validates the order of cars given the priority input.
    /// </summary>
    [TestClass]
    public class CarsControllerTest
    {
        /// <summary>
        /// A test method to validate retrieving all cars in the list
        /// </summary>
        [TestMethod]
        public void GetAllCars_ShouldReturnAllCars()
        {
            //Arrange            
            var controller = new CarsController();
            var testCars = controller.Cars;

            //Act
            var result = controller.GetAllCars() as List<Car>;

            //Assert
            Assert.AreEqual(testCars.Count, result.Count);
        }

        /// <summary>
        /// Test priority when the input is A-A-A
        /// </summary>
        [TestMethod]
        public void GetAllCars_A_Dash_A_Dash_AShouldReversePriority()
        {
            //Arrange            
            var controller = new CarsController();
            var testCars = controller.Cars;

            //Act
            var result = controller.GetCarsByPriority("A-A-A") as OkNegotiatedContentResult<List<Car>>;

            //Assert
            Assert.AreEqual(testCars[0].Id, result.Content[2].Id);
            Assert.AreEqual(testCars[1].Id, result.Content[1].Id);
            Assert.AreEqual(testCars[2].Id, result.Content[0].Id);
        }

        /// <summary>
        /// Test priority when the input is A-A
        /// </summary>
        [TestMethod]
        public void GetAllCars_A_Dash_AShouldReversePriorityInFirstTwo()
        {
            //Arrange            
            var controller = new CarsController();
            var testCars = controller.Cars;

            //Act
            var result = controller.GetCarsByPriority("A-A") as OkNegotiatedContentResult<List<Car>>;

            //Assert
            Assert.AreEqual(testCars[0].Id, result.Content[1].Id);
            Assert.AreEqual(testCars[1].Id, result.Content[0].Id);
            Assert.AreEqual(testCars[2].Id, result.Content[2].Id);
        }

        /// <summary>
        /// Test priority when the input is A*A
        /// </summary>
        [TestMethod]
        public void GetAllCars_AStarAShouldDoNothing()
        {
            //Arrange
            var controller = new CarsController();
            var testCars = controller.Cars;

            //Act
            var result = controller.GetCarsByPriority("A*A") as OkNegotiatedContentResult<List<Car>>;

            //Assert
            Assert.AreEqual(testCars[0].Id, result.Content[0].Id);
            Assert.AreEqual(testCars[1].Id, result.Content[1].Id);
            Assert.AreEqual(testCars[2].Id, result.Content[2].Id);
        }
    }
}
