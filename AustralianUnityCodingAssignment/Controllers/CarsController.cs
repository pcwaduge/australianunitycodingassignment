﻿using AustralianUnityCodingAssignment.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;

namespace AustralianUnityCodingAssignment.Controllers
{
    /// <summary>
    /// The WebApi has a single CarsController which has a defined list of Cars and
    /// api methods to retrieve a list of cars and change the order according to priority.
    /// </summary>
    public class CarsController : ApiController
    {
        /// <summary>
        /// A sample defined list of cars
        /// </summary>
        public List<Car> Cars = new List<Car>
        {
            new Car { Id = 1, Name = "Ford" },
            new Car { Id = 2, Name = "BMW" },
            new Car { Id = 3, Name = "Fiat" },            
        };

        /// <summary>
        /// Returns all cars.
        /// </summary>
        /// <returns>All cars</returns>
        [HttpGet]
        public IEnumerable<Car> GetAllCars()
        {
            return Cars;
        }

        /// <summary>
        /// Returns all cars given the priority.
        /// A-A-A: priority is reversed 
        /// A-A : priority is reversed in first 2
        /// A*A : do nothing
        /// </summary>
        /// <param name="priority">Priority</param>
        /// <returns>All cars</returns>
        [HttpGet]
        [ResponseType(typeof(List<Car>))]
        public IHttpActionResult GetCarsByPriority(string priority)
        {
            var cars = GetCarPriority(priority);
            if (cars != null)
            {
                return Ok(cars);
            }
            else
            {
                return NotFound();
            }
        }

        #region private methods

        /// <summary>
        /// A private method to change the order of priority for all cars.
        /// </summary>
        /// <param name="priority">Priority</param>
        /// <returns>All cars</returns>
        private List<Car> GetCarPriority(string priority)
        {
            if (string.IsNullOrEmpty(priority))
            {
                return null;
            }

            if (priority.ToUpper().Equals("A-A-A"))
            {
                return Cars.OrderByDescending(p => p.Id).ToList();
            }
            else if (priority.ToUpper().Equals("A-A"))
            {              
                return SwapValues<Car>(Cars, 0, 1);
            }
            else if (priority.ToUpper().Equals("A*A"))
            {
                return Cars;
            }

            return null;
        }

        /// <summary>
        /// A helper method to swap items in a list given two indexes.
        /// </summary>
        /// <typeparam name="T">Type of clas</typeparam>
        /// <param name="source">List of items</param>
        /// <param name="index1">first position</param>
        /// <param name="index2">second position</param>
        /// <returns>Returns swapped list</returns>
        private List<T> SwapValues<T>(IList<T> source, int index1, int index2)
        {            
            T[] array = new T[source.Count];
            source.CopyTo(array, 0);
            T temp = source[index1];
            array[index1] = source[index2];
            array[index2] = temp;
            return array.ToList();    
        }        

        #endregion
    }
}
