﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AustralianUnityCodingAssignment.Models
{
    /// <summary>
    /// A class to hold car related information
    /// </summary>
    public class Car
    {
        //Property to hold the sequence number in a list
        public int Id { get; set; }
        //Property to hold the name of the car brand
        public string Name { get; set; }
    }
}