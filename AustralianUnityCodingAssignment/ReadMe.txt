﻿===========================================================+=============
ASP.NET Web Api with index.html : AustralianUnityCodingAssignment Project Overview
========================================================================

Author: Prabhath Waduge
/////////////////////////////////////////////////////////////////////////////
Use:

  The project illustrates how to move things in a json file.
  
  If the input is

	A-A-A: priority is reversed 

	A-A : priority is reversed in first 2

	A*A : do nothing
	
Sample JSON is [ "Ford", "BMW", "Fiat" ] 

The WebApi has a single CarsController which has a defined list of Cars and
api methods to retrieve a list of cars and change the order according to priority.
The index.html page make ajax calls to retrieve json data and display it on the web page.

The test project validates the order of cars given the priority input.

/////////////////////////////////////////////////////////////////////////////
Prerequisite:

Visual Studio 2015 Update 3.
.NET Framework 4.5.2 

/////////////////////////////////////////////////////////////////////////////
To run the project:
  
*open the project in Visual Studio and hit F5. 

Please note, internet connection is required to download the NuGet packages and 
to load the jQuery library via the CDN for the index.html page. 

To run all the unit tests, go to Test -> Windows menu item to open the Test Explorer.

/////////////////////////////////////////////////////////////////////////////
Code Logical:

Step1. Create a Visual Studio 2015 Update 3 ASP.NET Web Application project

Step2. Use an empty template to add folder for Web Api and create unit test project

Step3: Add CarsController, Car model class and index.html web page files          
	  
Step4: Write unit tests